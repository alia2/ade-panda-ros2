
################################################################################
#
# Oh-my-zsh configuration
#
################################################################################

export ZSH=/usr/share/oh-my-zsh

# Set name of the theme to load. Optionally, if you set this to "random"
# it'll load a random theme each time that oh-my-zsh is loaded.
# See https://github.com/robbyrussell/oh-my-zsh/wiki/Themes
ZSH_THEME="bira"

DISABLE_AUTO_UPDATE="true"

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Add wisely, as too many plugins slow down shell startup.
plugins=(
    git
)

source $ZSH/oh-my-zsh.sh


################################################################################
#
# Setup the workspace
#
################################################################################

source /opt/ros/eloquent/setup.zsh

if [[ ! -d ~/ws_ros2/ ]]; then
    if [[ ! -f ~/build_workspace.steps.sh ]]; then
        cp /etc/skel/build_workspace.steps.sh ~/
    fi
    steps ~/build_workspace.steps.sh
fi

if [[ -d ~/ws_ros2/ ]]; then
  steps ~/build_workspace.steps.sh 6
fi

if [[ -f ~/ws_ros2/install/setup.zsh ]]; then
    source ~/ws_ros2/install/setup.zsh
fi

################################################################################
#
# User configuration
#
################################################################################

export LSCOLORS=ExGxCxDxBxegedabagacad

alias tree='tree -C'
alias sl='ls'
alias l='ls  -Alhrt'
alias l1='ls -1lhrt'
