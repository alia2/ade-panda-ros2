
mkdir -p ~/ws_ros2/src
cd ~/ws_ros2/src && git clone https://github.com/ros-planning/moveit2.git -b master
cd ~/ws_ros2/src && vcs import < moveit2/moveit2.repos || true
cd ~/ws_ros2/src && vcs import < moveit2/moveit_demo_nodes/run_moveit_cpp/run_moveit_cpp.repos
rosdep update
cd ~/ws_ros2/src && sudo apt-get update && rosdep install -r --from-paths . --ignore-src --rosdistro eloquent -y 
cd ~/ws_ros2 && colcon build --event-handlers desktop_notification- status- --cmake-args -DCMAKE_BUILD_TYPE=Release
source ~/ws_ros2/install/setup.sh
