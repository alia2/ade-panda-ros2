
# An ADE-compatible image
FROM registry.gitlab.com/deb0ch/ade-ubuntu/bionic-extra:latest


##
##  Install ros-indigo and dependencies
##

RUN apt-get update && apt-get -y install \
    git                                  \
    gnupg2                               \
    lsb-release                          \
    unzip                                \
    curl                                 \
    && rm -rf /var/lib/apt/lists/*

RUN locale-gen en_US en_US.UTF-8
RUN update-locale LC_ALL=en_US.UTF-8 LANG=en_US.UTF-8
RUN export LANG=en_US.UTF-8
RUN curl -s https://raw.githubusercontent.com/ros/rosdistro/master/ros.asc | sudo apt-key add -
RUN sh -c 'echo "deb http://packages.ros.org/ros2/ubuntu `lsb_release -cs` main" > /etc/apt/sources.list.d/ros2-latest.list'

 
RUN apt-get update && apt-get -y install \
    python3-rosdep                       \
    python3-colcon-common-extensions     \
    python3-vcstool                      \
    python3-argcomplete                  \
    ros-eloquent-desktop                 \
    && rm -rf /var/lib/apt/lists/*


RUN rosdep init

ENV GAZEBO_MODEL_PATH=/usr/share/gazebo/models
RUN git clone --depth=1 https://github.com/osrf/gazebo_models $GAZEBO_MODEL_PATH \
    && rm -rf $GAZEBO_MODEL_PATH/.git


##
## ADE
##

COPY env.sh /etc/profile.d/ade-wilselby_env.sh


##
## Copy local files into the system
##

COPY skel/. /
